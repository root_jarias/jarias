from flask import Flask, Blueprint
from technicalexercise.main.routes import main
from flask_material import Material


app = Flask(__name__)
Material(app)

app.register_blueprint(main)
