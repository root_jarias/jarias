from flask import Flask, render_template, Blueprint

main = Blueprint('main', __name__, template_folder='templates',
                 static_folder='static', static_url_path='/static/main')

# Routing
@main.route('/')
def base():
    tab_title = 'Home | T-E'

    return render_template('main/home.html', title=tab_title)


@main.route('/about_us')
def about_us():
    tab_title = 'About Us | T-E'
    about_us = [
        {
            'header': 'QCPU SAKAY',
            'sub_header': 'QCPU SAKAY - A Bus Transportation System',
            'content': '''Good Ride Starts Here. QCPU Sakay is your best commuting buddy that let you commute with ease. 
                        Get an all-in-one app that has Real Time Location Tracking with easy mode of payment. 
                        Just Ride, Locate, Tap and, '../static/img/banner-2.jpg' Scan to pay.''',
            'logo': 'https://websakay.web.app/components/images/logo-one.png',
            'background_color': '#31ccec',
            'background_image': 'none',
            'url': 'https://websakay.web.app/'
        },
        {
            'header': 'ServRevo',
            'sub_header': 'ServRevo Corporation - Seats and Room Reservation System',
            'content': '''ServRevo Corp. (ServRevo) is an outsourcing company providing shared services, co-working spaces, HR/Payroll, and staffing solutions.
                        Our bespoke offshore solutions integrate seamlessly with any business requirement ...''',
            'logo': 'http://www.servrevo.com/img/servrevo-logo.png',
            'background_color': '#21ba45',
            'background_image': 'http://www.servrevo.com/img/services/payroll-2.jpg',
            'url': 'http://www.servrevo.com/'
        },
    ]
    return render_template('main/about_us.html', title=tab_title, about_us=about_us)


@main.route('/contacts')
def contacts():
    tab_title = 'Contacts | T-E'
    return render_template('main/contacts.html', title=tab_title)
