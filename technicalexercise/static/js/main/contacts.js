 initGeolocation();

 mapboxgl.accessToken =
     'pk.eyJ1Ijoicm9vdC1qYXJpYXMiLCJhIjoiY2syazFkcmFhMTVrZTNubnZxOGRqd2s3ZCJ9.NkoN240EQVxj84Fbr2ZEaw'
 var map = new mapboxgl.Map({
     container: 'map',
     style: 'mapbox://styles/mapbox/light-v10',
     zoom: 3,
     center: [121.0173248, 14.594253]
 })

 function initGeolocation() {
     if (navigator.geolocation) {
         // Call getCurrentPosition with success and failure callbacks
         navigator.geolocation.getCurrentPosition(success, fail);
     } else {
         alert("Sorry, your browser does not support geolocation services.");
     }
 }

 function success(position) {
     if (position != null || position != undefined) {
         // Current Longitude and Latitude
         let long = position.coords.longitude;
         let lat = position.coords.latitude;

         // Home
         let origin = [121.017000, 14.671028];

         // My Current Location
         let destination = [long, lat];

         // initialize the map canvas 
         let canvas = map.getCanvasContainer();

         // An arbitrary 
         let start = [121.017000, 14.671028];

         // Make a directions request
         function getRoute(end) {
             // make a directions request using driving profile
             var start = [121.017000, 14.671028];
             var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + start[0] + ',' + start[1] + ';' +
                 end[0] +
                 ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;

             // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
             var req = new XMLHttpRequest();
             req.responseType = 'json';
             req.open('GET', url, true);
             req.onload = function () {
                 var data = req.response.routes[0];
                 var route = data.geometry.coordinates;
                 var geojson = {
                     type: 'Feature',
                     properties: {},
                     geometry: {
                         type: 'LineString',
                         coordinates: route
                     }
                 };
                 // if the route already exists on the map, reset it using setData
                 if (map.getSource('route')) {
                     map.getSource('route').setData(geojson);
                 } else {
                     map.addLayer({
                         id: 'route',
                         type: 'line',
                         source: {
                             type: 'geojson',
                             data: {
                                 type: 'Feature',
                                 properties: {},
                                 geometry: {
                                     type: 'LineString',
                                     coordinates: geojson
                                 }
                             }
                         },
                         layout: {
                             'line-join': 'round',
                             'line-cap': 'round'
                         },
                         paint: {
                             'line-color': '#009688',
                             'line-width': 5,
                             'line-opacity': 0.75
                         }
                     });
                 }
             };
             req.send();
         }

         map.on('load', function () {
             // make an initial directions request that
             getRoute(start);

             // Add starting point to the map
             map.loadImage('https://upload.wikimedia.org/wikipedia/commons/0/0a/Marker_location.png',
                 function (error, image) {
                     map.addImage('map-icon', image)
                     map.addLayer({
                         'id': 'points',
                         'type': 'symbol',
                         source: {
                             type: 'geojson',
                             data: {
                                 type: 'FeatureCollection',
                                 features: [{
                                     type: 'Feature',
                                     properties: {},
                                     geometry: {
                                         type: 'Point',
                                         coordinates: start
                                     }
                                 }]
                             }
                         },
                         'layout': {
                             'icon-image': 'map-icon',
                             'icon-size': 1
                         }
                     });
                 })

             var coords = [long, lat]
             //  let coords = [120.8298843, 15.4117242]

             let end = {
                 type: 'FeatureCollection',
                 features: [{
                     type: 'Feature',
                     properties: {},
                     geometry: {
                         type: 'Point',
                         coordinates: coords
                     }
                 }]
             };

             if (map.getLayer('end')) {
                 map.getSource('end').setData(end);
             } else {
                 map.addLayer({
                     id: 'end',
                     type: 'circle',
                     source: {
                         type: 'geojson',
                         data: {
                             type: 'FeatureCollection',
                             features: [{
                                 type: 'Feature',
                                 properties: {},
                                 geometry: {
                                     type: 'Point',
                                     coordinates: coords
                                 }
                             }]
                         }
                     },
                     paint: {
                         'circle-radius': 10,
                         'circle-color': '#f30'
                     }
                 });
             }
             getRoute(coords);

             // // Map fit 2 point coordinates
             var geojson = {
                 "type": "FeatureCollection",
                 "features": [{
                     "type": "Feature",
                     "geometry": {
                         "type": "LineString",
                         "properties": {},
                         "coordinates": [
                             [120.8298843, 15.4117242],
                             [long, lat],
                         ]
                     }
                 }]
             };

             // Geographic coordinates of the LineString
             var coordinates = geojson.features[0].geometry.coordinates;

             // Pass the first coordinates in the LineString to `lngLatBounds` &
             // wrap each coordinate pair in `extend` to include them in the bounds
             // result. A variation of this technique could be applied to zooming
             // to the bounds of multiple Points or Polygon geomteries - it just
             // requires wrapping all the coordinates with the extend method.
             var bounds = coordinates.reduce(function (bounds, coord) {
                 return bounds.extend(coord);
             }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

             map.fitBounds(bounds, {
                 padding: 80
             });

         });
     } else {
         fail();
     }

 }

 function fail() {
     alert('Could not obtain location');
 }