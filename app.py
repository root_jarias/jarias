from technicalexercise import app

# Start Application
if __name__ == '__main__':
    app.run(debug=True)
